#include<stdio.h>
#include<string.h>
#include<errno.h>
#include<stdlib.h>
#include<GL/glew.h>
#include<GLFW/glfw3.h>
#include <GL/gl.h>

int loadShader(const char * src, GLenum SHADER_TYPE)
{
        size_t length;
        int success;
        char log[256];
        GLuint shader  = glCreateShader(SHADER_TYPE);
        FILE *fp = fopen(src, "r");
        if(!fp){
                fprintf(stderr,"%s\n",strerror(errno));
                return -1;
        }
        fseek(fp,0,SEEK_END);
        length = ftell(fp);
        rewind(fp);
        // GLchar s[length];
        char *s = (char *)malloc(length+1);
        // char s[length+1];
        *(s+length) = 0;
        fread(s,1,length,fp);
        printf("%s\n",s);
        glShaderSource(shader,1,(char const * const *)&s,NULL);
        glCompileShader(shader);
        glGetShaderiv(shader,GL_COMPILE_STATUS,&success);
        if(!success){
                glGetShaderInfoLog(shader,256,NULL,log);
                fprintf(stderr,"Error while compiling shader %s:\n %s\n",src,log);
                return -1;
        }
        free(s);
        return shader;
}


void errorCallback(int code,const char *err)
{
        fprintf(stderr,"Error %d : \n %s\n",code,err);
}

GLuint createShaderProgram(GLuint vert, GLuint frag)
{
        int success;
        char log[256];
        GLuint shader_program = glCreateProgram();
        glAttachShader(shader_program, vert);
        glAttachShader(shader_program, frag);
        glLinkProgram(shader_program);
        glGetShaderiv(shader_program,GL_LINK_STATUS, &success);
        if(!success)
        {
                glGetShaderInfoLog(shader_program, 256, NULL, log);
                printf("Error linking shaders :\n %s\n",log);
                return -1;
        }
        glDeleteShader(vert);
        glDeleteShader(frag);
        return shader_program;
}

void resizeWindow(GLFWwindow *window, int width, int height)
{
        glViewport(0,0, width,height);
}



int main(){
	int width = 800, height = 400;
	GLuint vertex, fragment, program;
	GLuint VAO, VBO;

	glfwSetErrorCallback(errorCallback);
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	GLFWwindow *window = glfwCreateWindow(width, height, "New window", NULL, NULL);
	glfwMakeContextCurrent(window);
	glewInit();
	glfwSetFramebufferSizeCallback(window, resizeWindow);
	glViewport(0, 0, width, height);

	vertex = loadShader("./exe1-vert.glsl", GL_VERTEX_SHADER);
	fragment = loadShader("./exe1-frag.glsl", GL_FRAGMENT_SHADER);
	program = createShaderProgram(vertex, fragment);

	printf("VERTEX : %d\nFRAGMENT : %d\n PROGRAM : %d\n", vertex, fragment, program);
	float data[] = {
		0.0, 0.0, 0.0,
		0.5, 1.0, 0.0,
		0.5, -1.0, 0.0
	};

	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);
	glGenBuffers(1, &VBO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(data), data, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float)*3, (void *)0);
	glEnableVertexAttribArray(0);


	while(!glfwWindowShouldClose(window)){
		glClearColor(0.0, 0.3, 0.4, 1.0);
		glClear(GL_COLOR_BUFFER_BIT);
		glUseProgram(program);
		glBindVertexArray(VAO);
		glDrawArrays(GL_TRIANGLES, 0, 3);	

		glfwPollEvents();
		glfwSwapBuffers(window);
	}



	glfwTerminate();
	return 0;
}