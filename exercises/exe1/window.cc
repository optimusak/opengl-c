#include <cstddef>
#include <cstdio>
#include <fstream>
#include <iomanip>
#include<iostream>
#include <iterator>
#include <ostream>
#include <string>
#include<vector>
#include<GL/glew.h>
#include<GLFW/glfw3.h>


#ifdef __cplusplus
extern "C"{
#endif

typedef struct __worldObject{
    GLuint VAO;
    GLuint VBO;
    GLuint EBO;
} worldObject;

#ifdef __cplusplus
}
#endif

class Window{
public:

    int width,height;
    const char *caption;
    int success;
    void *data;
    GLuint VAO, VBO;
    GLuint currentProgram;
    std::vector<GLuint> programs;
    GLFWwindow *window;

    Window(int width, int height,const char *caption);
    void loop();
    void (*draw)(void *) = Window::notImplemented;
    GLuint loadShader(const char *, GLenum);
    GLuint createProgram(std::vector<GLuint> &);
    ~Window();

private:
    int initWindow();
    static void notImplemented(void *);
    static void resizeEverytime(GLFWwindow *window, int width, int height);
    static int ctx_major,ctx_minor;
    static void errorCallback(int status, const char *errstr);
};

int Window::ctx_major = 3;
int Window::ctx_minor = 3;


Window::Window(int width,int height,const char *title){
    this->width = width;
    this->height = height;
    this->caption = title;
    this->currentProgram = 0;
    this->draw = notImplemented;
    this->data = NULL;
    this->initWindow();
}

int Window::initWindow(){
    glfwSetErrorCallback(Window::errorCallback);
    if(!glfwInit()) return -1;
    
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR,this->ctx_major);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, this->ctx_minor);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    this->window = glfwCreateWindow(this->width, this->height, this->caption, NULL, NULL);
    if(!window){
        std::cerr << "error while initalizing window "<<std::endl;
        return -1;
    }

    glfwMakeContextCurrent(this->window);
    glewInit();
    glViewport(0,0,this->width,this->height);
    glfwSetFramebufferSizeCallback(this->window, this->resizeEverytime);
    return 1;
}

void Window::errorCallback(int status, const char *title){
    std::cout<<"Error Status : "<< status << std::endl
    << "Error Messsage : " << std::endl << title << std::endl;
}

void Window::resizeEverytime(GLFWwindow *window, int width, int height){
    glViewport(0, 0, width, height);
    std::cout << std::setw(10) << "WIDTH" << " " << std::setw(4) << width << std::endl
    << std::setw(10) << "HEIGHT" << " " << std::setw(4) << height << std::endl;
}


void Window::loop(){
    while(!glfwWindowShouldClose(this->window)){
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
        glClearColor(0.0f, 0.5f, 0.3f, 1.0f);
        // std::cout << this->currentProgram << std::endl;
        if((this->currentProgram) > 0){
            glUseProgram(currentProgram);
        }
        draw(this->data);
        // glBindVertexArray(0);
        glfwPollEvents();
        glfwSwapBuffers(this->window);
    }
}

Window::~Window(){
    glfwTerminate();
}

GLuint Window::loadShader(const char *name, GLenum shader_flag){
    std::ifstream shader_src(name);
    int logSize = 256;
    if(shader_src.is_open()){
        std::cout<< "Opened file successfully." << std::endl;
    }else{
        std::cout<<"Opening failed."<<std::endl;
        return -1;
    }

    std::istreambuf_iterator<char> iterator{shader_src}, end;
    std::string buffer(iterator, end);
    const char *buff = buffer.c_str();
    char errlog[logSize];

    GLenum shader = glCreateShader(shader_flag);
    glShaderSource(shader, 1, &buff, NULL);
    glCompileShader(shader);
    glGetShaderiv(shader, GL_COMPILE_STATUS, &this->success);
    if(!this->success){
        glGetShaderInfoLog(shader, logSize,NULL,errlog);
        std::cerr << "Error shader compile failed with error: "
        << std::endl << errlog << std::endl;
        return -1;
    }
    

    return shader;
}

GLuint Window::createProgram(std::vector<GLuint> &v){
    char logs[256];
    GLuint program = glCreateProgram();
    for(GLuint g : v){
        std::cout << g << std::endl;
        glAttachShader(program,g);
    }
    glLinkProgram(program);
    glGetShaderiv(program, GL_LINK_STATUS, &success);
    if(!success){
        glGetShaderInfoLog(program, 256, NULL, logs);
        std::cout <<" Shader linking failed with log"<< std::endl
        << logs << std::endl; 
        return -1;
    }
    return program;
}

void Window::notImplemented(void * data){ }


void getObject(worldObject &w, float *data, int size){
    GLuint VAO,VBO,EBO;
    int indicies[] = {
        0, 1, 2
    };
    glGenVertexArrays(1,&VAO);
    glBindVertexArray(VAO);
    glGenBuffers(1, &VBO);
    // for(int i=0; i < size/sizeof(float); i++) std::cout << data[i] << std::endl;
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER,size, data, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float)*3, (void *)0);

    // glGenBuffers(1, &EBO);
    // glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indicies), indicies, GL_STATIC_DRAW);
    // glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
    glEnableVertexAttribArray(0);
    w.EBO = EBO;
    w.VAO = VAO;
    w.VBO = VBO;
}

void draw(void *data){
    worldObject *obj = (worldObject *)data;
    // std::cout<< obj->VAO << std::endl;
    glBindVertexArray(obj->VAO);
    // glDrawElements(GL_TRIANGLES, 3, GL_INT, NULL);
    glDrawArrays(GL_TRIANGLES, 0, 3);
    // std::cout<<"Working.\n" << obj->VAO;
}

int main(int argc , char *argv[]){
    float data[] = {
        0.0, 0.5, 0,
        0.0, 0.5, 0,
        -0.5, 0.0, 0
    };


    std::vector<GLuint> shaders;
    GLuint shader;
    worldObject obj1;
    Window window(100,100,"This is it");

    shader = window.loadShader("./exe1-vert.glsl", GL_VERTEX_SHADER);
    shaders.push_back(shader);

    shader = window.loadShader("./exe1-frag.glsl", GL_FRAGMENT_SHADER);
    shaders.push_back(shader);

    window.currentProgram = window.createProgram(shaders);
    std::cout<< "Current Program " << window.currentProgram <<std::endl;
    getObject(obj1,data,sizeof(data));
    std::cout<< obj1.EBO << " "
    << obj1.VAO << " " << obj1.VBO << std::endl;
    window.data = &obj1;
    window.draw = draw;

    // glGenVertexArrays(1, &window.VAO);
    // glBindVertexArray(window.VAO);
    // glGenBuffers(1, &window.VBO);
    // glBindBuffer(GL_VERTEX_ARRAY, window.VBO);
    // glBufferData(GL_VERTEX_ARRAY, sizeof(data), data, GL_STATIC_DRAW);
    // glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float)*3, (void *)0);
    // glEnableVertexAttribArray(0);


    window.loop();

    return 0;
}