#include <errno.h>
#include<string.h>
#include<stdio.h>
#include<stdlib.h>
#include<GL/glew.h>
#include<GLFW/glfw3.h>
#include <sys/types.h>

typedef struct data
{
    GLFWwindow *window;
    int width,height,success;
    GLuint VBO, VAO, EBO;
    GLuint vertex_shader,fragment_shader,program;
    const char * caption;
} Data;

void errorCallback(int, const char *);
void setHints();
int loadShader(const char * src, GLenum SHADER_TYPE);
GLuint createShaderProgram(GLuint vert, GLuint frag);
void mainloop(Data *data);
void resizeWindow(GLFWwindow *window, int width, int height);
void closePress(Data *data);


int main(int argc,char *argv[])
{

    Data data = {.width=800, .height=400};
    data.caption = "GLFW Window";

    glfwSetErrorCallback(errorCallback);

    if (!glfwInit()) return -1;
    
    setHints();

    data.window = glfwCreateWindow(data.width, data.height, data.caption, NULL, NULL);
    
    if(!data.window) return -1;

    glfwMakeContextCurrent(data.window);
    glewInit();
    glViewport(0, 0, data.width, data.height);
    glfwSetFramebufferSizeCallback(data.window, resizeWindow);

    data.vertex_shader = loadShader("./exe1-vert.glsl", GL_VERTEX_SHADER);
    data.fragment_shader = loadShader("./exe1-frag.glsl", GL_FRAGMENT_SHADER);
    if(!(data.vertex_shader - data.fragment_shader)){
        data.program = createShaderProgram(data.vertex_shader, data.fragment_shader);
        glDeleteShader(data.vertex_shader);
        glDeleteShader(data.fragment_shader);
    }

    // glGenBuffers(1, &data.VAO);
    // glBindVertexArray(data.VAO);


    // mainloop(&data);
    while(!glfwWindowShouldClose(data.window))
    {
        closePress(&data);
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
        glClearColor(0.0f, 0.3f, 0.5f, 1.0f);
        glfwSwapBuffers(data.window);
        glfwPollEvents();
        // printf("Working.\n");
    }
    glfwTerminate();

    glfwTerminate();
}


void mainloop(Data *data){
    
}

void closePress(Data *data)
{
    if(glfwGetKey(data->window, GLFW_KEY_ESCAPE)) glfwSetWindowShouldClose(data->window, GLFW_TRUE);
}

void setHints()
{
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
}

void errorCallback(int status, const char * errStr)
{
    printf("Error %d:\n%s\n",status, errStr);
}

GLuint createShaderProgram(GLuint vert, GLuint frag)
{
        int success;
        char log[256];
        GLuint shader_program = glCreateProgram();
        glAttachShader(shader_program, vert);
        glAttachShader(shader_program, frag);
        glLinkProgram(shader_program);
        glGetShaderiv(shader_program,GL_LINK_STATUS, &success);
        if(!success)
        {
                glGetShaderInfoLog(shader_program, 256, NULL, log);
                printf("Error linking shaders :\n %s\n",log);
                return -1;
        }
        glDeleteShader(vert);
        glDeleteShader(frag);
        return shader_program;
}

void resizeWindow(GLFWwindow *window, int width, int height)
{
        glViewport(0,0, width,height);
}

int loadShader(const char * src, GLenum SHADER_TYPE)
{
        size_t length;
        int success;
        char log[256];
        GLuint shader  = glCreateShader(SHADER_TYPE);
        FILE *fp = fopen(src, "r");
        if(!fp){
                fprintf(stderr,"%s\n",strerror(errno));
                return -1;
        }
        fseek(fp,0,SEEK_END);
        length = ftell(fp);
        rewind(fp);
        // GLchar s[length];
        char *s = (char *)malloc(length+1);
        // char s[length+1];
        *(s+length) = 0;
        fread(s,1,length,fp);
        printf("%s\n",s);
        glShaderSource(shader,1,(char const * const *)&s,NULL);
        glCompileShader(shader);
        glGetShaderiv(shader,GL_COMPILE_STATUS,&success);
        if(!success){
                glGetShaderInfoLog(shader,256,NULL,log);
                fprintf(stderr,"Error while compiling shader %s:\n %s\n",src,log);
                return -1;
        }
        free(s);
        return shader;
}