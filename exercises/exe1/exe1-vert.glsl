#version 330 core
#ifdef GL_ES
precision mediump float;
#endif

layout(location=0) in vec3 vertex;
//layout(location=1) in vec3 color;

void main()
{
	gl_Position = vec4(vertex,1.0f);
}
