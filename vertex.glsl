#version 330 core
layout(location=0) in vec3 vertices;
layout(location=1) in vec3 color;
#define PI 3.141592653589793438
out vec3 fragColor;
mat4 rotate;
float val;
void main(){
    rotate =mat4(0.0,0.0,0.0,0.0,
                 0.0,0.0,0.0,0.0,
                 0.0,0.0,1.0,0.0,
                 0.0,0.0,0.0,1.0);
    rotate[0][0] = cos(PI/4);
    rotate[1][0] = -sin(PI/4);
    rotate[0][1] = sin(PI/4);
    rotate[1][1] = cos(PI/4);
    gl_Position = rotate * vec4(vertices,1.0f);
    fragColor = color;
}