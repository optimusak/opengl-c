#include<stdio.h>
#include<errno.h>
#include<string.h>
#include<stdlib.h>
#include<GL/glew.h>
#include<GLFW/glfw3.h>

int loadShader(const char *, GLenum);
void errorCallback(int,const char *);
GLuint createShaderProgram(GLuint vert, GLuint frag);
void resizeWindow(GLFWwindow *window, int width, int height);

GLuint createShaderProgram(GLuint vert, GLuint frag);
void resizeWindow(GLFWwindow *window, int width, int height);


int main(){

	GLFWwindow *window;
	int width = 800;
	int height = 400;
	const char *caption = "glfw window for rectangle";
	float data[12+12] = {
		-0.5,0.5,0.0,
		-0.5,-0.5,0.0,
		0.5,-0.5,0.0,
		0.5,0.5,0.0,
		1.0,1.0,	1.0,
		0.0,1.0,0.0,
		1.0,0.0,0.0,
		0.0,0.0,1.0,
	};

	int indicies[] = {
		0,1,2,
		2,3,0
	};

	glfwSetErrorCallback(errorCallback);
	glfwInit();
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
	glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
	
	window = glfwCreateWindow(width, height, caption, NULL, NULL);
	if(!window) 
	{
		glfwTerminate();
		return -1;
	}

	glfwSetFramebufferSizeCallback(window, resizeWindow);
	glfwSetFramebufferSizeCallback(window, resizeWindow);
	glfwMakeContextCurrent(window);
	if(!glewInit()){printf("GLEW INIT FAILED.\n");}

	glViewport(0,0, width,height);
	glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
	glViewport(0,0, width,height);

	GLuint vertex_shader = loadShader("vertex.glsl", GL_VERTEX_SHADER);
	GLuint fragment_shader = loadShader("fragment.glsl",GL_FRAGMENT_SHADER);
	printf("%d\n",vertex_shader);
	printf("%d\n",fragment_shader);

	GLuint shader = createShaderProgram(vertex_shader, fragment_shader);

	glUseProgram(shader);

	GLuint VAO,VBO,EBO;
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);
	glGenBuffers(1,&VBO);
	glGenBuffers(1, &EBO);

	glBindBuffer(GL_ARRAY_BUFFER,VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(data), data, GL_STATIC_DRAW);

	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float)*3, (void *)0);
	glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(float)*3, (void *)(4*12));

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indicies), indicies, GL_STATIC_DRAW);

	glEnableVertexAttribArray(0);
	glEnableVertexAttribArray(1);
	// glBindVertexArray(VAO);

	while(!glfwWindowShouldClose(window)){
		glClearColor(0.4f, 0.2f, 0.8f, 1.0f);
		glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
		glUseProgram(shader);
		glBindVertexArray(VAO);
		glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, (void *)0);
		// glDrawArrays(GL_TRIANGLES, 0, 3);
		glBindVertexArray(0);
		glfwPollEvents();
		glfwSwapBuffers(window);
	}

	
	glfwTerminate();





	return 0;
}



int loadShader(const char * src, GLenum SHADER_TYPE)
{
	size_t length;
	int success;
	char log[256];
	GLuint shader  = glCreateShader(SHADER_TYPE);
	FILE *fp = fopen(src, "r");
	if(!fp){
		fprintf(stderr,"%s\n",strerror(errno));
		return -1;
	}
	fseek(fp,0,SEEK_END);
	length = ftell(fp);
	rewind(fp);
	// GLchar s[length];
	char *s = (char *)malloc(length+1);
	// char s[length+1];
	*(s+length) = 0;
	fread(s,1,length,fp);
	printf("%s\n",s);
	glShaderSource(shader,1,(char const * const *)&s,NULL);
	glCompileShader(shader);
	glGetShaderiv(shader,GL_COMPILE_STATUS,&success);
	if(!success){
		glGetShaderInfoLog(shader,256,NULL,log);
		fprintf(stderr,"Error while compiling shader %s:\n %s\n",src,log);
		return -1;
	}	
	free(s);
	return shader;
}


void errorCallback(int code,const char *err)
{
	fprintf(stderr,"Error %d : \n %s\n",code,err);
}

GLuint createShaderProgram(GLuint vert, GLuint frag)
{
	int success;
	char log[256];
	GLuint shader_program = glCreateProgram();
	glAttachShader(shader_program, vert);
	glAttachShader(shader_program, frag);
	glLinkProgram(shader_program);
	glGetShaderiv(shader_program,GL_LINK_STATUS, &success);
	if(!success)
	{
		glGetShaderInfoLog(shader_program, 256, NULL, log);
		printf("Error linking shaders :\n %s\n",log);
		return -1;
	}
	glDeleteShader(vert);
	glDeleteShader(frag);
	return shader_program;
}

void resizeWindow(GLFWwindow *window, int width, int height)
{
	glViewport(0,0, width,height);
}