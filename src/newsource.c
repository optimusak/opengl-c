#include <time.h>
#include<stdio.h>
#include<math.h>
#include<stdlib.h>
#include<GL/glew.h>
#include<GLFW/glfw3.h>

#define PI 3.14159265358979323846264338
//Shader code;
// #define RAND_MAX 1.0

const char *vertex_source = "#version 330 core\n"
    "layout(location=0) in vec3 vertices;\n"
    "layout(location=1) in vec3 colors;\n"
    "out vec4 fragColor;\n"
    "void main(){\n"
    "    gl_Position = vec4(vertices,1.0);\n"
    "    fragColor = vec4(colors,1.0);\n"
    "}\n\0";

const char *fragment_source = "#version 330 core\n"
    "out vec4 color;\n"
    "in vec4 fragColor;\n"
    "void main(){color = fragColor;}\n\0";


//error handling prototype;
void handleError(int status,const char *errmsg);
int create_shader(const char *string,GLenum shader_type);
void resize(GLFWwindow *window, int width,int height);

int main(int argc, const char *argv[])
{

    srand(time(NULL));
    GLFWwindow *window;
    int width = 800,height = 400;
    int status;
    char logs[256];
    float r = 300;
    float angle = 2.0*PI/3;
    GLfloat vertices[18];
    for(int i=0; i<18/3;i++){
        if(i>2){
            vertices[i*3+0] = 1.0*rand()/RAND_MAX;
            vertices[i*3+1] = 1.0*rand()/RAND_MAX;
            vertices[i*3+2] = 1.0*rand()/RAND_MAX;
            continue;
        }
        vertices[i*3+0] = r*sin(angle*i)/width;
        vertices[i*3+1] = r*cos(angle*i)/height;
        vertices[i*3+2] = 1.0;
    }

    glfwSetErrorCallback(handleError);

    if(!glfwInit()) return -1;

    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR,3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR,3);
    glfwWindowHint(GLFW_OPENGL_PROFILE ,GLFW_OPENGL_CORE_PROFILE);

    window = glfwCreateWindow(width, height, "opengl window", NULL,  NULL);
    
    if(!window){
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glewInit();
    glViewport(0,0,width/2,height/2);

    glfwSetFramebufferSizeCallback(window, resize);
    
    printf("wowkring nrw\n");
    GLuint vertex_shader = create_shader(vertex_source, GL_VERTEX_SHADER);
    GLuint fragment_shader = create_shader(fragment_source, GL_FRAGMENT_SHADER);
    GLuint program = glCreateProgram();
    glAttachShader(program,vertex_shader);
    glAttachShader(program,fragment_shader);
    glLinkProgram(program);
    glGetShaderiv(program, GL_LINK_STATUS, &status);
    if(!status)
    {
        glGetShaderInfoLog(program,256 , NULL, logs);
        printf("Error while linking shaders : \n%s\n",logs);
        glfwTerminate();
        return -1;
    }

    glUseProgram(program);
    glDeleteShader(vertex_shader);
    glDeleteShader(fragment_shader);

    GLuint VBO,VAO;

    glGenBuffers(1,&VBO);
    glGenVertexArrays(1, &VAO);
    glBindVertexArray(VAO);
    glBindBuffer(GL_ARRAY_BUFFER, VBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(vertices) , vertices, GL_STATIC_DRAW);
    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(float)*3, (void *)0);
    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(float)*3, (void *)(sizeof(float)*9));
    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(1);
    while(!glfwWindowShouldClose(window)){
        glClearColor(0.0f,0.04f,0.1f,1.0f);
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);

        glDrawArrays(GL_TRIANGLES, 0, 3);

        glfwSwapBuffers(window);
        glfwPollEvents();
    }

    glfwTerminate();

    return 0;
}

//error handling function.
void handleError(int status,const char *errmsg)
{
    printf("Error : %d : %s\n",status, errmsg);
}

void resize(GLFWwindow *window,int width,int height)
{
    glViewport(0, 0, width/2, height/2);
}

int create_shader(const char *string,GLenum shader_type)
{
    char logs[256];
    int success;
    printf("working 3\n");
    GLuint shader = glCreateShader(shader_type);
    printf("working 2\n");
    glShaderSource(shader,1,&string,NULL);
    glCompileShader(shader);
    glGetShaderiv(shader,GL_COMPILE_STATUS,&success);
    printf("working\n");
    if(!success) {
        glGetShaderInfoLog(shader,256,NULL,logs);
        printf("Compile Error : \n%s\n", logs);
        glfwTerminate();
        exit(1);
    }
    return shader;
}