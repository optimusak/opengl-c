#include<stdio.h>
#include<stdlib.h>
#include "glad.h"
#include<GLFW/glfw3.h>


const char *vertexShaderSource = "#version 330 core\n"
    "layout (location = 0) in vec3 aPos;\n"
    "void main()\n"
    "{\n"
    "   gl_Position = vec4(aPos, 1.0);\n"
    "}\0";

const char *fragmentShaderSource = "#version 330 core\n"
    "out vec4 FragColor;\n"
    "\nvoid main()"
    "{\n"
        "FragColor = vec4(1.0f, 0.5f, 0.2f, 1.0f);\n"
    "}\0";

void glfwErrorHandler(int status,const char *str){
    printf("ERROR %d :: %s\n",status,str);
}

void processKey(GLFWwindow *window){
    int key = glfwGetKey(window, GLFW_KEY_ESCAPE);
    if(key) glfwSetWindowShouldClose(window, 1);
}

void resizeViewPort(GLFWwindow *window,int width,int height) {
    glViewport(0, 0, width, height);
}

float inputs[] = {
     0.5f, 0.5f, 0.0,
    -0.5f, 0.5f,-0.0,
     0.0f,-0.5f, 0.0
};

unsigned int VBO,VAO;

int main(){
    int width = 360;
    int height = 720;
    int success;
    char infoLog[512];

    glfwSetErrorCallback(glfwErrorHandler);
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR,3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR,3);
    glfwWindowHint(GLFW_OPENGL_PROFILE,GLFW_OPENGL_CORE_PROFILE);
    GLFWwindow *window = glfwCreateWindow(width, height, "New Speech", NULL, NULL);
    if(window == NULL) {
        glfwTerminate();
        return -1;
    }

    glfwMakeContextCurrent(window);

    if(!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress)){
        printf("Failed to initialize glad.\n");
        return -1;
    }

    glfwSetWindowSizeCallback(window, resizeViewPort);
    glViewport(0, 0, width, height);

    glad_glGenBuffers(1,&VBO);
    glad_glBindBuffer(GL_ARRAY_BUFFER,VBO);
    glad_glBufferData(VBO,sizeof(inputs),inputs,GL_STATIC_DRAW);


    unsigned int vertexShader,fragmentShader;

    vertexShader = glad_glCreateShader(GL_VERTEX_SHADER);
    glad_glShaderSource(vertexShader,1,&vertexShaderSource,NULL);
    glad_glCompileShader(vertexShader);

    glad_glGetShaderiv(vertexShader,GL_COMPILE_STATUS, &success);
    printf("%d\n",success);
    if(!success){
        glGetShaderInfoLog(vertexShader,sizeof(infoLog), NULL, infoLog);
        printf("Vertex Shader compile error %s\n",infoLog);
        return -1;
    }

    fragmentShader = glad_glCreateShader(GL_FRAGMENT_SHADER);
    glad_glShaderSource(fragmentShader,1,&fragmentShaderSource,NULL);
    glad_glCompileShader(fragmentShader);

    glad_glGetShaderiv(fragmentShader,GL_COMPILE_STATUS, &success);
    printf("%d\n",success);
    if(!success){
        glGetShaderInfoLog(fragmentShader,sizeof(infoLog), NULL, infoLog);
        printf("fragment Shader compile error %s\n",infoLog);
        return -1;
    }

    unsigned int shaderProgram;
    shaderProgram = glad_glCreateProgram();
    glAttachShader(shaderProgram,vertexShader);
    glad_glAttachShader(shaderProgram,fragmentShader);
    glLinkProgram(shaderProgram);
    glGetProgramiv(shaderProgram,GL_LINK_STATUS,&success);
    if(!success){
        glGetProgramInfoLog(shaderProgram,sizeof(infoLog), NULL, infoLog);
        printf("Shader Link compile error %s\n",infoLog);
        return -1;
    }

    glUseProgram(shaderProgram);

    glDeleteShader(vertexShader);
    glDeleteShader(fragmentShader);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, GL_FLOAT*3, (void *)0);
    glEnableVertexAttribArray(0);

    glGenVertexArrays(1,&VAO);
    glBindVertexArray(VAO);
    

    while(!glfwWindowShouldClose(window)){
        processKey(window);
        glClearColor(0.0,0.4,0.3,1.0);
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
        glDrawArrays(GL_TRIANGLES,0,3);
        glfwSwapBuffers(window);
        glfwPollEvents();
    }
    glfwTerminate();

    return 0;
}