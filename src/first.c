#include<stdio.h>
#include<stdlib.h>
#include<GL/gl.h>
#include<GLFW/glfw3.h>
#include <sys/types.h>
#define WIDTH 640
#define HEIGHT 480

void resizeWindow(GLFWwindow *window,int width,int height){
    // printf("INFO : WIDTH: %d HEIGHT: %d\n");
    glViewport(0, 0, width, height);
}

void processKey(GLFWwindow *window){
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS){
        glfwSetWindowShouldClose(window, GLFW_TRUE);
    }
    // printf("%d %d\n",glfwGetKey(window, GLFW_KEY_A),glfwGetKey(window, GLFW_KEY_D));

}

void printGLFWError(int errno,const char *errstr){
    fprintf(stderr,"Error %d : %s\n",errno,errstr);
}

int main(int argc,char *argv[]){
    int width,height;
    glfwSetErrorCallback(printGLFWError);
    if(!glfwInit()){
        fprintf(stderr,"Error while initiating glfw\n");
        exit(1);
    }
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR,  3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);
    glfwWindowHint(GLFW_TRANSPARENT_FRAMEBUFFER, GLFW_TRUE);
    glfwWindowHint(GLFW_DECORATED, GLFW_FALSE);
    glfwWindowHint(GLFW_SCALE_TO_MONITOR, GLFW_TRUE);
    GLFWwindow *window;
    GLFWmonitor *m;
    m = glfwGetPrimaryMonitor();
    window = glfwCreateWindow(WIDTH, HEIGHT, "First GL Window", NULL, NULL);
    if(window == NULL){
        fprintf(stderr, "Error while creating GLFW Window.\n");
        glfwTerminate();
        exit(1);
    }
    glfwSetWindowPos(window, 0, 0);
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, resizeWindow);
    glfwGetWindowSize(window, &width, &height);
    glViewport(0, 0, width, height);
    glfwSetWindowAttrib(window, GLFW_DECORATED, GLFW_FALSE);

    while(!glfwWindowShouldClose(window)){
        processKey(window);
        glfwPollEvents();
        glClearColor(0.05f, 0.32f, 0.5f, 0.0f);
        glClear(GL_COLOR_BUFFER_BIT|GL_DEPTH_BUFFER_BIT);
        glfwSwapBuffers(window);
    }
    glfwTerminate();
    return 0;
}